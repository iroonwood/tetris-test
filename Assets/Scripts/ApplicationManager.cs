﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ApplicationManager : MonoBehaviour
{
    private static ApplicationManager _instance;

    private ApplicationManager() { }

    public static ApplicationManager instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<ApplicationManager>(); ;
            }

            return _instance;
        }
    }

    public void LoadMenu()
    {
        SceneManager.LoadScene("Main Menu");
    }

    public void LoadSceneMode1()
    {
        SceneManager.LoadScene("Scene Mode 1");
    }

    public void LoadSceneMode2()
    {
        SceneManager.LoadScene("Scene Mode 2");
    }

    public void LoadSceneGameOver()
    {
        SceneManager.LoadScene("Game Over");
    }

    public void QuitGame()
    {
        #if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
        #else
	        Application.Quit();
        #endif
    }
}
