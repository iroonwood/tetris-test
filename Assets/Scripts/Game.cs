﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game : MonoBehaviour
{
    const float MinimalTimeToFall = 0.1f;

    [Header("Game properties")]
    [SerializeField, Range(10, 100)]        private int    _height      = 20;
    [SerializeField, Range(10, 100)]        private int    _width       = 10;
    [SerializeField, Range(1, 20)]          private int    _targetLines = 1;
    [SerializeField, Range(0.1f, 100.0f)]   private float  _timeToFall  = 1.0f;
    [SerializeField, Range(0.001f, 1.0f)]   private float  _timeToFallDelta = 0.001f;
    [SerializeField]                        private bool   _wallsStrict = true;

    [Header("Tetramino spawn properties")]
    [SerializeField] private Vector2        _spawnPoint     = Vector2.zero;
    [SerializeField] private TetraminoList  _tetraminoList  = null;


    protected float               _previousFallTime = 0.0f;
    protected IContainerInterface _minoContainer    = null;
    protected Tetramino           _currentTetramino = null;

    public int  height  { get { return _height;  } }
    public int  width   { get { return _width;  } }
    public bool wallsStrict { get { return _wallsStrict; } }
    public float timeToFall { get { return _timeToFall; } }


    void Start()
    {
        _minoContainer = new Grid(_width, _height);

        SpawnNewTetramino();
    }

    void Update()
    {
        if (_currentTetramino)
        {
            if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                _currentTetramino.Move(true, _width, wallsStrict);
            }
            else if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                _currentTetramino.Move(false, _width, wallsStrict);
            }
            else if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                _currentTetramino.Rotate(true, width, wallsStrict);
            }
            else if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                _currentTetramino.Rotate(false, width, wallsStrict);
            }

            if (!IsCurrentTetraminoValid())
            {
                _currentTetramino.RestorePositions();
            }

            if (Input.GetKeyDown(KeyCode.Space) || Time.time - _previousFallTime >= timeToFall)
            {
                _currentTetramino.MoveDown();

                if (!IsCurrentTetraminoValid())
                {
                    _currentTetramino.RestorePositions();

                    UpdateGrid();
                    CheckLines();
                    DespawnCurrentTetramino();
                    SpawnNewTetramino();
                }

                _previousFallTime = Time.time;
            }
        }
    }
    
    public void UpdateGrid()
    {
        if (_currentTetramino != null)
        {
            _minoContainer.InsertInGrid(_currentTetramino.GetChildren());
        }
    }

    protected void CheckLines()
    {
        int linesCount   = 0;
        int line = 0;
        int targetHeight = _height;

        while (line < targetHeight)
        {
            if (_minoContainer.IsFullLine(line))
            {
                ++linesCount;
            }
            else
            {
                if (linesCount >= _targetLines)
                {
                    line -= linesCount;
                    targetHeight -= linesCount;

                    for (int i = line; i < line + linesCount; ++i)
                    {
                        _minoContainer.ClearLine(i);
                    }

                    MoveLineDown(line, linesCount);
                    IncreaseSpeed();
                }

                linesCount = 0;
            }

            ++line;
        }
    }

    protected void MoveLineDown(int line, int linesCount)
    {
        if (line < 0 || linesCount < 0 || line + linesCount >= _height)
        {
            return;
        }


        for (int m = line + linesCount; m < _height; ++m)
        {
            for (int j = 0; j < _width; ++j)
            {
                if (_minoContainer[j, m] != null)
                {
                    _minoContainer[j, m - linesCount] = _minoContainer[j, m];
                    _minoContainer[j, m] = null;
                    _minoContainer[j, m - linesCount].position -= new Vector3(0, linesCount, 0);
                }
            }
        }
    }

    protected bool IsCurrentTetraminoValid()
    {
        if (_currentTetramino == null)
        {
            return false;
        }

        foreach (Transform child in _currentTetramino.transform)
        {
            if (child.position.y >= height)
            {
                continue;
            }

            if (!_minoContainer.InGrid(child.position))
            {
                return false;
            }

            if (!_minoContainer.IsCellFree(child.position))
            {
                return false;
            }
        }

        return true;
    }

    private void DespawnCurrentTetramino()
    {
        if (_currentTetramino)
        {
            _currentTetramino.GetChildren().ForEach((child) => child.parent = this.transform);
             
            Destroy(_currentTetramino.gameObject);
            _currentTetramino = null;
        }
    }

    private void SpawnNewTetramino()
    {
        GameObject tetraminoPref = _tetraminoList.GetTetramino();

        if (tetraminoPref)
        {
            _currentTetramino = Instantiate(tetraminoPref, _spawnPoint, Quaternion.identity).GetComponent<Tetramino>();
            _currentTetramino.moveUnderHeight(_height);

            if (!IsCurrentTetraminoValid())
            {
                GameOver();
            }
        }
    }

    private void IncreaseSpeed()
    {
        _timeToFall = Mathf.Max(_timeToFall - _timeToFallDelta, MinimalTimeToFall);
    }

    private void GameOver()
    {
        enabled = false;

        if (ApplicationManager.instance != null)
        {
            ApplicationManager.instance.LoadSceneGameOver();
        }
    }
}
