﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grid: IContainerInterface
{
    private int _width, _height = 0;
    private Transform[,] _grid;


    public Grid(int width, int height)
    {
        _width = width;
        _height = height;

        _grid = new Transform[width, height];
    }

    public int width { get { return _width; } }

    public int height { get { return _height; } }

    public Transform this[int x, int y]
    {
        get
        {
            if (x >= 0 && x < _width && y >= 0 && y < _height)
            {
                return _grid[x, y];
            }

            return null;
        }
        set
        {
            if (x >= 0 && x < _width && y >= 0 && y < _height)
            {
                _grid[x, y] = value;
            }
        }
    }

    public bool InGrid(Vector2 position)
    {
        int x = Mathf.FloorToInt(position.x);
        int y = Mathf.FloorToInt(position.y);

        return (x >= 0 && x < _width && y >= 0);
    }

    public bool IsCellFree(Vector2 position)
    {
        int x = Mathf.FloorToInt(position.x);
        int y = Mathf.FloorToInt(position.y);

        if (x < 0 || x >= _width || y < 0 || y >= _height)
        {
            return false;
        }

        return _grid[x, y] == null;
    }

    public bool IsFullLine(int line)
    {
        for (int j = 0; j < _width; ++j)
        {
            if (_grid[j, line] == null)
            {
                return false;
            }
        }

        return true;
    }

    public void ClearLine(int line)
    {
        if (line > _height || line < 0)
        {
            return;
        }

        for (int j = 0; j < _width; ++j)
        {
            Transform mino = _grid[j, line];

            if (mino != null)
            {
                MonoBehaviour.Destroy(mino.gameObject);
            }

            _grid[j, line] = null;
        }
    }

    public void InsertInGrid(Transform mino)
    {
        int x = Mathf.RoundToInt(mino.position.x);
        int y = Mathf.RoundToInt(mino.position.y);

        _grid[x, y] = mino;
    }

    public void InsertInGrid(List<Transform> minos)
    {
        foreach (Transform mino in minos)
        {
            InsertInGrid(mino);
        }
    }
}
