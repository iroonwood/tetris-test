﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IContainerInterface
{
    int width { get; }

    int height { get; }

    Transform this[int x, int y]
    {
        get;
        set;
    }

    bool InGrid(Vector2 position);

    bool IsCellFree(Vector2 position);

    bool IsFullLine(int line);

    void ClearLine(int line);

    void InsertInGrid(Transform mino);

    void InsertInGrid(List<Transform> minos);
}
