﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransfromInfo
{
    public Vector3 position;
    public Quaternion rotation;

    public TransfromInfo(Vector3 position, Quaternion rotation)
    {
        this.position = position;
        this.rotation = rotation;
    }
}

public class Tetramino : MonoBehaviour
{
    [SerializeField] protected Transform _centerMino;

    protected Dictionary<int, TransfromInfo> _positions = new Dictionary<int, TransfromInfo>();

    public virtual void Move(bool isLeft, int width, bool strict)
    {
        SavePositions();

        transform.position += isLeft ? new Vector3(-1, 0, 0) : new Vector3(1, 0, 0);

        if (!strict)
        {
            Teleport(width);
        }
    }

    public virtual void MoveDown()
    {
        SavePositions();

        transform.position += new Vector3(0, -1.0f, 0);
    }

    public void moveUnderHeight(int height)
    {
        foreach (Transform child in transform)
        {
            if (child.position.y >= height)
            {
                transform.position -= new Vector3(0, child.position.y - height + 1, 0);
            }
        }
    }

    protected void Teleport (int width)
    {
        foreach (Transform child in transform)
        {
            if (child.position.x >= width)
            {
                child.position = new Vector3(child.position.x % width, child.position.y, child.position.z);
            }
            else if (child.position.x < 0)
            {
                child.position = new Vector3(child.position.x + width, child.position.y, child.position.z);
            }
        }
    }

    public virtual void Rotate(bool left, int gridWidth, bool strict = true)
    {
        SavePositions();

        foreach (Transform child in transform)
        {
            Vector2 position = child.position;
            Vector2 center = _centerMino.position;

            if (!strict)
            {
                float diff = position.x - center.x;

                if (Mathf.Abs(diff) >= gridWidth / 2)
                {
                    if (Mathf.Sign(diff) < 0)
                    {
                        center.x = center.x - gridWidth;
                    }
                    else
                    {
                        center.x = gridWidth + center.x;
                    }
                }
            }

            Debug.Log(child.name + " Rotate:" + position + " Center:" + center);

            child.RotateAround(center, new Vector3(0, 0, 1), left ? 90.0f : -90.0f);

            if (strict)
            {
                if (child.position.x >= gridWidth)
                {
                    transform.position -= new Vector3(1 + child.position.x - gridWidth, 0, 0);
                }
                else if (child.position.x < 0)
                {
                    transform.position += new Vector3(-child.position.x, 0, 0);
                }
            }
        }

        if (!strict)
        {
            Teleport(gridWidth);
        }
    }

    public void SavePositions()
    {
        foreach (Transform child in transform)
        {
            if (_positions.ContainsKey(child.GetInstanceID()))
            {
                _positions[child.GetInstanceID()].position = child.position;
                _positions[child.GetInstanceID()].rotation = child.rotation;
            }
            else
            {
                _positions.Add(child.GetInstanceID(), new TransfromInfo(child.position, child.rotation));
            }
        }
    }

    public void RestorePositions()
    {
        foreach (Transform child in transform)
        {
            if (_positions.ContainsKey(child.GetInstanceID()))
            {
                child.position = _positions[child.GetInstanceID()].position;
                child.rotation = _positions[child.GetInstanceID()].rotation;
            }
        }
    }

    public List<Transform> GetChildren()
    {
        List<Transform> children = new List<Transform>();

        foreach (Transform child in transform)
        {
            children.Add(child);
        }

        return children;
    }
}
