﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ProbabilityMinoPair
{
    public float probability;
    public GameObject tetramino;
}

[CreateAssetMenu(fileName = "New Tetramino List")]
public class TetraminoList : ScriptableObject, ISerializationCallbackReceiver
{
    [SerializeField]
    private List<ProbabilityMinoPair> _tetraminos = new List<ProbabilityMinoPair>();
 
    private bool _actualized = false;
    protected List<ProbabilityMinoPair> _actualTetraminos = new List<ProbabilityMinoPair>();

    protected float _proportionSummary = 0.0f;


    public GameObject GetTetramino()
    {
        if (!_actualized)
        {
            Actualize();
        }

        if (_actualTetraminos.Count > 0)
        {
            float choose = Random.Range(0, _proportionSummary);
            float accumulatedProbability = 0;

            foreach (ProbabilityMinoPair minoPair in _actualTetraminos)
            {
                accumulatedProbability += minoPair.probability;

                if (choose <= accumulatedProbability)
                {
                    return minoPair.tetramino;
                }
            }
        }

        return null;
    }

    private void Actualize()
    {
        foreach (ProbabilityMinoPair tetraminoPair in _tetraminos)
        {
            if (tetraminoPair != null && tetraminoPair.probability > 0.0f && tetraminoPair.tetramino != null)
            {
                _actualTetraminos.Add(tetraminoPair);
                _proportionSummary += tetraminoPair.probability;
            }
        }
        _actualized = true;
    }

    public void OnAfterDeserialize()
    {
        _actualized = false;
        _proportionSummary = 0.0f;
        _actualTetraminos.Clear();
    }

    public void OnBeforeSerialize()
    {

    }
}
