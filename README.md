# Tetris Test

Educational test unity project

## Controls

```
Left Arrow  - move left  
Right Arrow - move right  
Up Arrow    - rotate left  
Down Arrow  - rotate right  
Space       - move down  
```

### Prerequisites

```
Unity 2019.3.0f6
```
